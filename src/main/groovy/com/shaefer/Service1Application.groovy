package com.shaefer

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class Service1Application {

	static void main(String[] args) {
		SpringApplication.run Service1Application, args
	}
}
