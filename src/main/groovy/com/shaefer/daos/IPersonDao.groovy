package com.shaefer.daos

/**
 * Created by dshaefer on 1/13/17.
 */
interface IPersonDao {

    def getPeopleByLastName(String lastName)
}