package com.shaefer.daos

import org.flywaydb.core.internal.dbsupport.JdbcTemplate
import org.springframework.beans.factory.annotation.Autowired

/**
 * Created by dshaefer on 1/13/17.
 */
class PersonDao implements IPersonDao {

    @Autowired JdbcTemplate jdbcTemplate

    @Override
    getPeopleByLastName(String lastName) {
        jdbcTemplate.queryForList("select * from Person where last_name = ?", lastName)
    }
}
