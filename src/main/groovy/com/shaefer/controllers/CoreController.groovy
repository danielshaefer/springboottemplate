package com.shaefer.controllers

import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController

@RestController
class CoreController {

    @RequestMapping("/")
    @ResponseBody
    String home() {
        return "Hello World!"
    }

}
