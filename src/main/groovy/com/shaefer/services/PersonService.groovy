package com.shaefer.services

import com.shaefer.daos.IPersonDao
import com.shaefer.models.Person
import org.springframework.beans.factory.annotation.Autowired

/**
 * Created by dshaefer on 1/13/17.
 */
class PersonService implements IPersonService {

    @Autowired IPersonDao personDao

    @Override
    List<Person> getPeopleByLastName(String lastName) {
        personDao.getPeopleByLastName(lastName)
    }
}
