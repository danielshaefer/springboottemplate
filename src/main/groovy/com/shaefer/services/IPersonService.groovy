package com.shaefer.services

import com.shaefer.models.Person

/**
 * Created by dshaefer on 1/13/17.
 */
interface IPersonService {

    List<Person> getPeopleByLastName(String lastName)

}