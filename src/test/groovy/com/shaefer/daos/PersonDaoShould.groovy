package com.shaefer.daos

import org.flywaydb.core.internal.dbsupport.JdbcTemplate
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.runners.MockitoJUnitRunner

import static org.mockito.Mockito.*
import static org.junit.Assert.*

@RunWith(MockitoJUnitRunner)
class PersonDaoShould {

    @Mock JdbcTemplate jdbcTemplate
    @InjectMocks PersonDao personDao

    @Test void makeDatabaseCall() {

        def lastName = "test"
        def peopleList = []
        when(jdbcTemplate.queryForList("select * from Person where last_name = ?", lastName))
                .thenReturn(peopleList)

        def result = personDao.getPeopleByLastName(lastName)

        assertEquals(peopleList, result)

    }

}
