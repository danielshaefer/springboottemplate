package com.shaefer.services

import com.shaefer.daos.IPersonDao
import com.shaefer.models.Person
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.runners.MockitoJUnitRunner

import static org.junit.Assert.assertEquals
import static org.mockito.Mockito.when

@RunWith(MockitoJUnitRunner)
class PersonServiceShould {

    @Mock private IPersonDao personDao
    @InjectMocks private PersonService personService

    @Test
    void GetPeopleByLastName() {
        def expectedPerson = new Person(lastName: "test")
        def peopleList = [expectedPerson]
        when(personDao.getPeopleByLastName(expectedPerson.lastName)).thenReturn(peopleList)

        assertEquals(1, peopleList.size())
        assertEquals(peopleList[0].lastName, personService.getPeopleByLastName(expectedPerson.lastName)[0].lastName)
    }

}
