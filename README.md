# Spring Boot Web Template #
While there are lots of nice tools now to get started and generate a basic Spring Boot Web Project there is always some basic work of getting an example class or two with tests exposed.
This project simply provides some basics with tests

## What is assumed? ##
* Gradle Build System
* Groovy as base language (although adding in straight Java is very simple)
* Spring Boot as base

## What Spring Dependencies (and other Dependencies are included)? ##
### Core stuff ###
* Spring Boot Starter Web
* Spring Boot Starter Test (Mockito and more)
### Database Stuff ###
* Spring JDBC
* H2
* Flyway (db migration stuff)

## What Boilerplate is included ##
 * One autoconfigured Controller class
 * One model class
 * One Table in the Database
 * One Record in the Table
 * One Service class
 * One DAO class
 * Tests for Controller, Service, DAO and core Spring Boot Start
